package gui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Launcher extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        VBox pane = FXMLLoader.load(
                Launcher.class.getResource("/gui/prompt.fxml")
        );
        pane.setMinSize(493, 371);
        Scene scene = new Scene(pane);
        primaryStage.setScene(scene);
        primaryStage.setTitle("TEST GENERATOR");
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

}
