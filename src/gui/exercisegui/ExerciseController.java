package gui.exercisegui;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class ExerciseController implements Initializable, UserResponseObserver {
    private static final int MIN_DIGITS = 1;
    private static final int MAX_DIGITS = 25;
    private static final int MIN_BASE = 2;
    private static final int MAX_BASE = 16;

    private ExerciseModel model = new ExerciseModel();

    @FXML
    private GridPane root;

    @FXML
    private TextField startingBase, endingBase, inputSize, outputField;

    @FXML
    private Label generated, correctStats, incorrectStats, unansweredStats;


    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    private void updateStatLabels() {
        correctStats.setText("TOČNO: " + model.getCorrectAnswers());
        incorrectStats.setText("NETOČNO: " + model.getIncorrectAnswers());
        unansweredStats.setText("NEODGOVORENO: " + model.getUnanswered());
    }

    private void launchAlert(String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("POGREŠKA");
        alert.setHeaderText("");
        alert.setContentText(message);
        alert.showAndWait();
    }

    private void emptyStringAlert(String additionalInfo) {
        launchAlert("Niste unijeli " + additionalInfo);
    }

    @Override
    public void onOutputTestRequested(ExerciseModel model) {
        model.testCorrectness(outputField.getText());
    }

    @Override
    public void onNextTaskRequested(ExerciseModel model) {
        int startingBase, endingBase, inputSize;
        try {
            startingBase = Integer.parseInt(this.startingBase.getText());
            if (startingBase < MIN_BASE || startingBase > MAX_BASE) {
                launchAlert(
                                  "Nedopuštena početna baza. Dozvoljene vrijednosti su " +
                                  "prirodni brojevi unutar intervala " +
                                          "[" + MIN_BASE + ", " + MAX_BASE + "]"
                );
            }
        } catch (NumberFormatException e) {
            if (this.startingBase.getText().isEmpty()) {
                emptyStringAlert("početnu bazu!");
            } else {
                launchAlert("'" + this.startingBase.getText() + "' nije broj!");
            }
            return;
        }

        try {
            endingBase = Integer.parseInt(this.endingBase.getText());
            if (endingBase < MIN_BASE || endingBase > MAX_BASE) {
                launchAlert(
                                "Nedopuštena završna baza. Dozvoljene vrijednosti su " +
                                "prirodni brojevi unutar intervala " +
                                "[" + MIN_BASE + ", " + MAX_BASE + "]"
                );
            }
        } catch (NumberFormatException e) {
            if (this.endingBase.getText().isEmpty()) {
                emptyStringAlert("završnu bazu!");
            } else {
                launchAlert("'" + this.endingBase.getText() + "' nije broj!");
            }
            return;
        }

        if (startingBase == endingBase) {
            launchAlert("Prelagan zadatak :)");
            return;
        }

        try {
            inputSize = Integer.parseInt(this.inputSize.getText());

            if (inputSize < MIN_DIGITS || inputSize > MAX_DIGITS) {
                launchAlert(
                                "Veličina početnog broja treba biti prirodni broj unutar intervala " +
                                "[" + MIN_DIGITS + ", " + MAX_DIGITS + "]!"
                );
                return;
            }

        } catch (NumberFormatException e) {
            if (this.inputSize.getText().isEmpty()) {
                emptyStringAlert("veličinu početnog broja!");
            } else {
                launchAlert("'" + this.inputSize.getText() + "' nije broj!");
            }
            return;
        }

        model.next(startingBase, endingBase, inputSize);

        generated.setText(model.getInputNumber());

        updateStatLabels();
    }

    @FXML
    public void test() {
        try {
            onOutputTestRequested(model);
            updateStatLabels();
        } catch (NullPointerException e) {

        }
    }

    @FXML
    public void next() {
        onNextTaskRequested(model);
    }

    @FXML
    public void back() {
        ((Stage) root.getScene().getWindow()).close();
    }

}