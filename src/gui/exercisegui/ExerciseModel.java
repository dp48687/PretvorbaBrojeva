package gui.exercisegui;

import static numberconverter.NumberConversionGenerator.generateSubTask;

public class ExerciseModel {

    private String history;
    private String[] currentTask;
    private int correctAnswers;
    private int incorrectAnswers;
    private int unanswered;
    private int tasksGenerated;
    private boolean tested;


    public void next(int baseFrom, int baseTo, int inputLength) {
        currentTask = generateSubTask(baseFrom, baseTo, inputLength);
        ++tasksGenerated;

        if (tasksGenerated - (incorrectAnswers + correctAnswers + unanswered) >= 2) {
            ++unanswered;
        }

        history = null;
    }

    public void testCorrectness(String userOutput) {
        if (userOutput.replace(" ", "").equals(currentTask[1]) && history == null) {
            ++correctAnswers;
            history = currentTask[1];
        } else if (history == null) {
            ++incorrectAnswers;
        }
    }

    public String getInputNumber() {
        return currentTask[0];
    }

    public int getCorrectAnswers() {
        return correctAnswers;
    }

    public int getIncorrectAnswers() {
        return incorrectAnswers;
    }

    public int getUnanswered() {
        return unanswered;
    }

}
