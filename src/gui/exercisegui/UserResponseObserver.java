package gui.exercisegui;

public interface UserResponseObserver {

    void onOutputTestRequested(ExerciseModel model);

    void onNextTaskRequested(ExerciseModel model);

}
