package numberconverter;

import test.general.TextFillTask;

import java.util.Random;

public class NumberConversionGenerator {
    private static Random random = new Random();
    private static final char[] DIGITS = new char[]{
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
            'A', 'B', 'C', 'D', 'E', 'F'
    };

    private static String randomNumber(int base, int length) {
        StringBuilder builder = new StringBuilder();

        builder.append(DIGITS[1 + Math.abs(random.nextInt() % (base - 1))]);
        for (int i = 1; i < length; ++i) {
            builder.append(DIGITS[Math.abs(random.nextInt() % base)]);
        }

        return builder.toString();
    }

    public static TextFillTask randomTask(int subTasks) {
        StringBuilder taskBuilder = new StringBuilder();
        StringBuilder solutionBuilder = new StringBuilder();

        for (int i = 0; i < subTasks; ++i) {
            int baseFrom = randomBase();
            int baseTo;
            while ((baseTo = randomBase()) == baseFrom) {

            }
            int min;
            if (baseFrom < 4) {
                min = 8;
            } else if (baseFrom < 8) {
                min = 6;
            } else if (baseFrom < 12) {
                min = 4;
            } else {
                min = 3;
            }
            String generated = randomNumber(baseFrom, randomLength(min, min + 1));

            solutionBuilder.append(new String(NumberConverter.convert(generated, baseFrom, baseTo)));
            solutionBuilder.append('\n');


            appendExtraSpaces(taskBuilder, 2);
            taskBuilder.append(generated);
            taskBuilder.append(" (");
            taskBuilder.append(baseFrom);
            taskBuilder.append(")");
            appendExtraSpaces(taskBuilder, 13 - generated.length());
            taskBuilder.append('=');
            appendExtraSpaces(taskBuilder, 13 - generated.length());
            taskBuilder.append("_______________  (");
            taskBuilder.append(baseTo);
            taskBuilder.append(')');
            if (i < subTasks - 1) {
                taskBuilder.append("\n");
            }
        }

        return new NumberConversionTask(taskBuilder.toString(), solutionBuilder.toString().split("\n"));
    }

    private static int randomBase() {
        return 2 + Math.abs(random.nextInt() % 15);
    }

    private static int randomLength(int min, int max) {
        return (int) (min + (random.nextFloat()) * (max - min));
    }

    private static void appendExtraSpaces(StringBuilder builder, int quantity) {
        for (int i = 0; i < quantity; ++i) {
            builder.append(' ');
        }
    }

    public static String[] generateSubTask(int baseFrom, int baseTo, int inputLength) {
        String generated = randomNumber(baseFrom, inputLength);
        String solution = new String(NumberConverter.convert(generated, baseFrom, baseTo));
        return new String[]{generated, solution};
    }

}
