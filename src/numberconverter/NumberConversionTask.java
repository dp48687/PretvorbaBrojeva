package numberconverter;

import test.general.TextFillTask;

public class NumberConversionTask extends TextFillTask {

    public NumberConversionTask(String question, String[] correct) {
        super(question, correct, 2, 0, "pretvorba brojeva");
    }

}
