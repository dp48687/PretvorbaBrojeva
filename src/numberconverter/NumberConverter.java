package numberconverter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class NumberConverter {

    public static char[] convert(String input, int inputBase, int outputBase) {
        char[] inputCharContent = reverseString(Objects.requireNonNull(input)).toCharArray();

        validateBase(inputBase);
        validateBase(outputBase);
        validateContent(inputCharContent, inputBase);

        char[] base10Content = convertToBase10(inputCharContent, inputBase);

        return outputBase == 10 ? base10Content : convertFromBase10(base10Content, outputBase);
    }

    private static char[] convertToBase10(char[] input, int inputBase) {
        long x = 0;
        for (int i = 0; i < input.length; ++i) {
            x += charToDigit(input[i]) * potentionOfX(inputBase, i);
        }

        List<Character> converted = new ArrayList<>();
        do {
            converted.add(digitToChar(x % 10));
            x /= 10;
        } while (x > 0);

        Collections.reverse(converted);
        return converted.toString()
                .replace("[", "")
                .replace("]", "")
                .replace(", ", "")
                .toCharArray();
    }

    private static char[] convertFromBase10(char[] input, int finalBase) {
        long converted = array10ToNumber(input);

        List<Character> digits = new ArrayList<>();
        do {
            digits.add(digitToChar(converted % finalBase));
            converted /= finalBase;
        } while (converted > 0);

        Collections.reverse(digits);
        return digits.toString()
                .replace("[", "")
                .replace("]", "")
                .replace(", ", "")
                .toCharArray();
    }

    private static long array10ToNumber(char[] input) {
        long converted = 0;
        for (int i = 0; i < input.length; ++i) {
            converted += charToDigit(input[i]) * potentionOfX(10, i);
        }
        return converted;
    }

    private static long potentionOfX(int base, int exponent) {
        if (exponent == 0) {
            return 1;
        } else {
            return base * potentionOfX(base, exponent - 1);
        }
    }

    private static int charToDigit(char input) {
        return input >= 'A' && input <= 'F' ? 10 + input - 'A' : input - '0';
    }

    private static char digitToChar(long digit) {
        return digit > 9 ? (char) (digit - 10 + 'A') : (char) (digit + '0');
    }

    private static void validateBase(int base) {
        if (base < 2 || base > 16) {
            throw new IllegalArgumentException(
                    "Invalid base: " + base + ", must be in [2, 16]."
            );
        }

    }

    private static void validateContent(char[] content, int base) {
        int counter = 0;
        for (char digit : content) {
            if (digit >= '0' && digit <= '9') {
                if (digit - '0' >= base) {
                    throw new IllegalArgumentException(
                            "Invalid digit at position " + counter + ": '" + digit + "'" +
                                    ". Valid: [" + 0 + ", " + (base - 1) + "]"
                    );
                }
            } else if (digit >= 'A' && digit <= 'F') {
                if (digit - 'A' + 10 >= base) {
                    throw new IllegalArgumentException(
                            "Invalid digit at position " + counter + ": '" + digit + "'" +
                                    ". Valid: [" + 0 + ", " + (base - 1) + "]"
                    );
                }
            } else {
                throw new IllegalArgumentException(
                        "Invalid digit at position " + counter + ": '" + digit + "'"
                );
            }
            ++counter;
        }
    }

    private static String reverseString(String content) {
        if (content == null) {
            return null;
        } else {
            StringBuilder builder = new StringBuilder();
            for (int i = content.length() - 1; i >= 0; --i) {
                builder.append(content.charAt(i));
            }
            return builder.toString();
        }
    }

}
