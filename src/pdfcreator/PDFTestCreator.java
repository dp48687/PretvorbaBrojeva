package pdfcreator;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import numberconverter.NumberConversionGenerator;
import numberconverter.NumberConversionTask;
import test.Test;
import test.general.MultipleChoiceTask;
import test.general.ShortAnswerTask;
import test.general.Task;
import test.general.TextFillTask;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PDFTestCreator {
    private float paragraphLineSpacing = 12;
    private float cellLineSpacing = 1.45f;
    private float questionFont = 8;
    private float warningFont = 10;
    private float headerFont = 10;
    private float mainHeadingFont = 16;

    private static Font HEADER_FONT_DATA;
    private static Font HEADER_FONT_HEADING;
    private static Font ANSWER_PANE_FONT;
    private static Font WARNING_FONT;
    private static Font QUESTION_FONT;

    private PdfPTable headerTable;


    {
        BaseFont baseFont = null;
        BaseFont baseFontNormal = null;
        BaseFont baseFontBig = null;
        BaseFont baseFontGreekLettersSupported = null;

        try {
            baseFontGreekLettersSupported = BaseFont.createFont(
                    BaseFont.HELVETICA, "CP1253", false
            );
            baseFont = BaseFont.createFont(
                    BaseFont.HELVETICA, BaseFont.CP1250, false
            );
            baseFontNormal = BaseFont.createFont(
                    BaseFont.HELVETICA, BaseFont.CP1250, false
            );
            baseFontBig = BaseFont.createFont(
                    BaseFont.HELVETICA_BOLD, BaseFont.CP1250, false
            );
        } catch (Exception ignorable) {

        }

        QUESTION_FONT = new Font(baseFont, questionFont, Font.BOLD);
        ANSWER_PANE_FONT = new Font(baseFont, questionFont, Font.NORMAL);
        WARNING_FONT = new Font(baseFontNormal, warningFont, Font.ITALIC);
        HEADER_FONT_DATA = new Font(baseFontNormal, headerFont, Font.NORMAL);
        HEADER_FONT_HEADING = new Font(baseFontBig, mainHeadingFont, Font.BOLD);

    }


    public PDFTestCreator() {

    }

    public PDFTestCreator(Map<String, Float> data) {
        if (data.containsKey("paragraphLineSpacing")) {
            paragraphLineSpacing = data.get("paragraphLineSpacing");
        }
        if (data.containsKey("cellLineSpacing")) {
            cellLineSpacing = data.get("cellLineSpacing");
        }
        if (data.containsKey("questionFont")) {
            questionFont = data.get("questionFont");
        }
        if (data.containsKey("warningFont")) {
            warningFont = data.get("warningFont");
        }
        if (data.containsKey("headerFont")) {
            headerFont = data.get("headerFont");
        }
        if (data.containsKey("mainHeadingFont")) {
            mainHeadingFont = data.get("mainHeadingFont");
        }
    }

    /**
     * Creates a PDF document and saves it on a directory.
     * @param filename the path to the new PDF document
     * @throws DocumentException
     * @throws IOException
     */
    private void createTestPdf(String filename, Test t, boolean showQuestionHeading) throws Exception {
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream(filename));
        createHeaderTable(t, false);


        document.open();
        document.add(headerTable);
        document.add(createEmptySpace(1));
        document.add(createWarningParagraph(t.getAttribute("warning").toString()));
        document.add(createEmptySpace(1));


        int[] counter = new int[]{0};
        t.forEachTask(task -> {
            try {
                document.add(createQuestionBox(++counter[0], task, showQuestionHeading));
                document.add(createEmptySpace(1));
            } catch (DocumentException e) {

            }
        });

        document.close();

    }

    private void createResultsPdf(String filename, Test t, boolean showQuestionHeading) throws Exception {
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream(filename));
        createHeaderTable(t, true);

        document.open();
        document.add(headerTable);
        document.add(createEmptySpace(3));

        int[] counter = new int[]{0};
        t.forEachTask(task -> {
            try {
                document.add(createResultsBox(++counter[0], task, showQuestionHeading));
                document.add(createEmptySpace(1));
            } catch (DocumentException e) {

            }
        });

        document.close();
    }

    private void createHeaderTable(Test test, boolean areResults) throws DocumentException {
        String mainHeaderContent = test.getAttribute("testName").toString() + (areResults ? "\nRJEŠENJA" : "");

        headerTable = new PdfPTable(4);

        headerTable.setWidthPercentage(100);
        headerTable.setWidths(new float[]{2.8f, 3.5f, 10, 2});

        headerTable.addCell(createCell(1, 1, "DATUM", Element.ALIGN_LEFT, HEADER_FONT_DATA));
        headerTable.addCell(createCell(1, 1, test.getAttribute("date").toString(), Element.ALIGN_LEFT, HEADER_FONT_DATA));
        headerTable.addCell(createCell(1, 7, mainHeaderContent, Element.ALIGN_CENTER, HEADER_FONT_HEADING));
        PdfPCell qrCode = createCell(1, 7, null, Element.ALIGN_CENTER, HEADER_FONT_DATA);
        qrCode.setVerticalAlignment(Element.ALIGN_MIDDLE);
        qrCode.addElement(createQRCode(null));
        headerTable.addCell(qrCode);

        headerTable.addCell(createCell(1, 1, "RAZRED", Element.ALIGN_LEFT, HEADER_FONT_DATA));
        headerTable.addCell(createCell(1, 1, test.getAttribute("class").toString(), Element.ALIGN_LEFT, HEADER_FONT_DATA));
        headerTable.addCell(createCell(1, 1, "GRUPA", Element.ALIGN_LEFT, HEADER_FONT_DATA));
        headerTable.addCell(createCell(1, 1, test.getAttribute("group").toString(), Element.ALIGN_LEFT, HEADER_FONT_DATA));

        headerTable.addCell(createCell(1, 2, "PROFESOR/ICA", Element.ALIGN_LEFT, HEADER_FONT_DATA));
        headerTable.addCell(createCell(1, 1, test.getAttribute("professor").toString(), Element.ALIGN_LEFT, HEADER_FONT_DATA));
        headerTable.addCell(createCell(1, 1, " ", Element.ALIGN_LEFT, HEADER_FONT_DATA));
        headerTable.addCell(createCell(1, 2, "UČENIK/CA", Element.ALIGN_LEFT, HEADER_FONT_DATA));
        headerTable.addCell(createCell(1, 1, test.getAttribute("student").toString(), Element.ALIGN_LEFT, HEADER_FONT_DATA));
        headerTable.addCell(createCell(1, 1, " ", Element.ALIGN_LEFT, HEADER_FONT_DATA));

    }

    private Paragraph createEmptySpace(int quantity) {
        String empty = "";

        for (int i = 0; i < quantity; ++i) {
            empty += "\n";
        }

        return new Paragraph(empty);
    }

    private Paragraph createWarningParagraph(String warning) {
        Paragraph warningParagraph = new Paragraph(paragraphLineSpacing, warning, WARNING_FONT);
        warningParagraph.setAlignment(Element.ALIGN_CENTER);
        return warningParagraph;
    }

    private Image createQRCode(Test test) throws DocumentException {
        BarcodeQRCode barcodeQRCode = new BarcodeQRCode(
                "sample", 15, 15, null
        );
        Image codeQrImage = barcodeQRCode.getImage();
        codeQrImage.scaleAbsolute(50, 50);
        codeQrImage.setAlignment(Element.ALIGN_CENTER);
        return codeQrImage;
    }

    private PdfPCell createCell(int colspan, int rowspan, String text, int alignment, Font font) {
        PdfPCell cell = new PdfPCell();

        if (text != null && !text.isEmpty()) {
            Paragraph toInsert = new Paragraph(text, font);
            toInsert.setAlignment(alignment);
            cell.addElement(toInsert);
        }

        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setColspan(colspan);
        cell.setRowspan(rowspan);
        cell.setLeading(0, cellLineSpacing);

        return cell;
    }

    private PdfPTable createSimpleTable(int columns, float[] widths, float widthPercentage) {
        PdfPTable simpleTable = new PdfPTable(columns);

        try {
            simpleTable.setWidths(widths);
        } catch (DocumentException e) {

        }
        simpleTable.setWidthPercentage(widthPercentage);

        return simpleTable;
    }

    private PdfPTable createQuestionBox(int index, Task task, boolean showQuestionHeading) {
        PdfPTable questionTable;

        PdfPCell cell;

        Paragraph questionParagraph;
        String questionToDisplay = showQuestionHeading ? "ZADATAK " + Integer.toString(index) + "\n" : "\n";
        String answerPane;

        if (!(task instanceof MultipleChoiceTask)) {
            questionTable = createSimpleTable(1, new float[]{1.0f}, 100);

            questionToDisplay += task instanceof ShortAnswerTask ?
                    task.getAttribute("concreteQuestion").toString() :
                    task.getAttribute("question").toString();

            questionParagraph = new Paragraph(questionToDisplay + "\n\n", QUESTION_FONT);

            if (task instanceof ShortAnswerTask) {
                answerPane = task.getAttribute("answerPane").toString() + "\n";
                questionParagraph.add(new Chunk(answerPane, ANSWER_PANE_FONT));
            }

            cell = new PdfPCell(questionParagraph);
            cell.setLeading(0, cellLineSpacing);
            questionTable.addCell(cell);

        } else {
            questionTable = createSimpleTable(2, new float[]{1.0f, 10.0f}, 100);
            questionTable.addCell(createCell(2, 1, task.getAttribute("question").toString(), Element.ALIGN_LEFT, QUESTION_FONT));

            List<String> choices = ((MultipleChoiceTask) task).getChoices();
            for (int i = 0; i < choices.size(); ++i) {
                questionTable.addCell(createCell(1, 1, " ", Element.ALIGN_LEFT, ANSWER_PANE_FONT));
                questionTable.addCell(createCell(1, 1, choices.get(i), Element.ALIGN_LEFT, ANSWER_PANE_FONT));
            }
        }

        return questionTable;
    }

    private PdfPTable createResultsBox(int index, Task task, boolean showQuestionHeading) {
        PdfPTable questionTable = createSimpleTable(1, new float[]{1.0f}, 100);

        String answerToDisplay = showQuestionHeading ? "ZADATAK " + Integer.toString(index) + ":\n\n" : "";
        answerToDisplay += task.getAttribute("steps").toString();

        Paragraph questionParagraph = new Paragraph(answerToDisplay, QUESTION_FONT);

        PdfPCell cell = new PdfPCell(questionParagraph);
        cell.setLeading(0, cellLineSpacing);
        questionTable.addCell(cell);

        return questionTable;
    }

    public static void generate(Test test, String path, int amount, boolean generateResults, boolean showQuestionHeading) {
        PDFTestCreator pdfTestCreator = new PDFTestCreator();

        for (int i = 0; i < amount; ++i) {
            String name = test.getAttribute("testName").toString() + "_1.pdf";
            String testName = generateTestName(test, path);
            String resultsName = generateResultsName(testName, path);

            test.permutateQuestions();

            try {
                pdfTestCreator.createTestPdf(
                        Paths.get(path).resolve(name).toAbsolutePath().toString(), test, showQuestionHeading
                );
                if (generateResults) {
                    pdfTestCreator.createResultsPdf(
                            Paths.get(path).resolve(resultsName).toAbsolutePath().toString(), test, showQuestionHeading
                    );
                }
            } catch (Exception e) {

            }
        }
    }

    public static void generate(Test[] test, String path, boolean generateResults, boolean showQuestionHeading) {
        PDFTestCreator pdfTestCreator = new PDFTestCreator();

        for (Test testUnit: test) {

            String testName = generateTestName(testUnit, path);
            String resultsName = generateResultsName(testName, path);

            try {
                pdfTestCreator.createTestPdf(
                        Paths.get(path).resolve(testName).toAbsolutePath().toString(), testUnit, showQuestionHeading
                );
                if (generateResults) {
                    pdfTestCreator.createResultsPdf(
                            Paths.get(path).resolve(resultsName).toAbsolutePath().toString(), testUnit, showQuestionHeading
                    );
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static String generateTestName(Test testUnit, String path) {
        Path p = Paths.get(path);

        String name = testUnit.getAttribute("testName").toString() + "_1.pdf";
        int iteration = 1;
        while (Files.exists(p.resolve(name))) {
            name = testUnit.getAttribute("testName").toString() +
                    "_" + Integer.toString(++iteration) +
                    ".pdf";
        }

        return name;
    }

    private static String generateResultsName(String testName, String path) {
        Path p = Paths.get(path);

        String resultsName = testName.replace(".pdf", "") + "_RJEŠENJA.pdf";
        String extraAppend = "";
        while (Files.exists(p.resolve(resultsName))) {
            extraAppend = extraAppend + "_";
            resultsName = testName.replace(".pdf", "") +
                    "_RJEŠENJA" + extraAppend +
                    ".pdf";
        }

        return resultsName;
    }

}
