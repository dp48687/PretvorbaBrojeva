package test.general;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public abstract class MultipleChoiceTask extends Task {
    protected List<String> choices;
    protected List<String> correct;

    public MultipleChoiceTask(String question,
                              List<String> choices,
                              List<String> correct,
                              int pointsCorrect,
                              int pointsWrong,
                              String tag) {
        super(question, null, pointsCorrect, pointsWrong, tag);
        this.choices = Objects.requireNonNull(choices);
        if (choices.size() > 14) {
            throw new IllegalArgumentException(
                    "Maximum allowed answers: 14"
            );
        }
        this.correct = correct;
        this.answer = correct.toString()
                .replace("[", "")
                .replace("]", "")
                .replace(", ", "\n");

        Collections.shuffle(this.choices);
    }

    @Override
    protected Object makeAnswer() {
        return correct;
    }

    public List<String> getChoices() {
        return choices;
    }

    @Override
    public Object getAttribute(String identifier) {
        try {
            return super.getAttribute(identifier);
        } catch (IllegalArgumentException e1) {
            if (identifier.equals("choices")) {
                return choices;
            } else {
                throw e1;
            }
        }
    }

}
