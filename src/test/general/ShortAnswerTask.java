package test.general;

public abstract class ShortAnswerTask extends Task {
    private String question;
    private double[] inputData;
    private String[] units;

    public ShortAnswerTask(String question,
                           String answerPane,
                           double[] inputData,
                           String[] units,
                           int pointsCorrect,
                           int pointsWrong,
                           String tag) {
        super(question, answerPane, pointsCorrect, pointsWrong, tag);
        this.originalQuestion = question;
        this.inputData = inputData;
        this.units = units;

        onInputDataChanged();
    }

    @Override
    protected Object makeAnswer() {
        return null;
    }

    public abstract Object generateAnswer(double[] inputData, String[] units);

    public abstract Object generateSteps(double[] inputData, String[] units);

    public void setInputData(double[] inputData) {
        this.inputData = inputData;
        onInputDataChanged();
    }

    public void setUnits(String[] units) {
        this.units = units;
        onInputDataChanged();
    }

    private void onInputDataChanged() {
        this.question = originalQuestion + "";
        for (Double data : inputData) {
            this.question = this.question.replaceFirst("_value_", Double.toString(data));
        }
        for (String unit : units) {
            this.question = this.question.replaceFirst("_unit_", unit);
        }
        answer = generateAnswer(inputData, units);
    }

    @Override
    public Object getAttribute(String identifier) {
        try {
            return super.getAttribute(identifier);
        } catch (IllegalArgumentException e1) {
            if (identifier.equals("concreteQuestion")) {
                return question;
            } else {
                throw e1;
            }
        }
    }

    @Override
    public String toString() {
        return question + "\n" + answerPane;
    }

}
