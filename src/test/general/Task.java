package test.general;

import java.util.Objects;

public abstract class Task {

    protected String tag;
    protected String originalQuestion;
    protected Object answer;
    protected Object answerPane;
    protected Object steps;
    protected int pointsCorrect;
    protected int pointsWrong;


    public Task(String originalQuestion, Object answerPane, int pointsCorrect, int pointsWrong, String tag) {
        this.tag = tag;
        this.originalQuestion = originalQuestion;
        this.answerPane = answerPane;
        this.pointsCorrect = pointsCorrect;
        this.pointsWrong = pointsWrong;
        this.answer = makeAnswer();
    }

    protected abstract Object makeAnswer();

    public Object getAttribute(String identifier) {
        Objects.requireNonNull(identifier);
        switch (identifier) {
            case "tag":
                return tag;
            case "question":
                return originalQuestion;
            case "answer":
                return answer;
            case "steps":
                return steps != null ? steps : answer;
            case "pointsCorrect":
                return pointsCorrect;
            case "pointsWrong":
                return pointsWrong;
            case "answerPane":
                return answerPane;
            default:
                throw new IllegalArgumentException(
                        "Unknown identifier: '" +
                                identifier +
                                "'. Possible values: tag, question, concreteQuestion, " +
                                "answer, pointsCorrect, pointsWrong."
                );
        }
    }

}
